package v1.dao;

import java.io.Serializable;
import java.time.LocalDate;

public class Person implements Serializable {
	// private
	// default (package)
	// protected
	// public
	
	protected int id;
	protected String name;
	protected String address;
	protected LocalDate birthDate;
	
	public int shared;
	
	public Person(int id, String name, String address, LocalDate birthDate) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.birthDate = birthDate;
		shared = 1;
	}
	
	@Override
	public String toString() {
		return getClass() + ": id=" + id + " name=" + name 
				+ " address=" + address + " birthDate=" + birthDate; 
	}
}