import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

import v1.dao.Person;

public class TestAppl2 {
	public static void main(String[] args) {
		// Create
		Person person = 
				new Person(1, "name1", "Address1", LocalDate.now());
		
		// Write try-catch < Java7 
//		FileOutputStream fos = null;
//		ObjectOutputStream output = null;
//		try {
//			// FileNotFounException
//			fos = new FileOutputStream("myFile.data");
//			// IOException
//			output = new ObjectOutputStream(fos);
//			// IOException
//			output.writeObject(person);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			// IOException
//			try {
//				fos.close();
//				output.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		
		// Write try-catch >= Java7 
		try (FileOutputStream fos = new FileOutputStream("myFile.data");
				ObjectOutputStream output = new ObjectOutputStream(fos)) {
			output.writeObject(person);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Read
		Person person2 = null;
		try (FileInputStream fis = new FileInputStream("myFile.data");
				ObjectInputStream input = new ObjectInputStream(fis)) {
			person2 = (Person)input.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		// Print
		System.out.println(person2);
	}
}