import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

import v1.dao.Person;

public class TestAppl {
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		// Create
		Person person = 
				new Person(1, "name1", "Address1", LocalDate.now());
		
		// Write
		FileOutputStream fos = new FileOutputStream("myFile.data");
		ObjectOutputStream output = new ObjectOutputStream(fos);
		output.writeObject(person);
		output.close();
		
		
		// Read
		FileInputStream fis = new FileInputStream("myFile.data");
		ObjectInputStream input = new ObjectInputStream(fis);
		Person person2 = (Person)input.readObject();
		input.close();
		
		// Print
		System.out.println(person2);
	}
}