package v1.dao;
import java.time.LocalDate;

import v1.dao.Person;

public class Employee extends Person {
	int salary;
	public int shared;

	public Employee(int id, String name, String address, LocalDate birthDate, int salary) {
		super(id, name, address, birthDate);
		this.salary = salary;
		shared = 2;
	}
	
	@Override
	public String toString() {
		return super.toString() + " salary=" + salary; 
	}
	
}