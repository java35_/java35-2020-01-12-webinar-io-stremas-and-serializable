package v1.appl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import v1.dao.Person;

public class Appl {
	public static void main(String[] args) {
		List<Person> persons = getRandomPersons();
		
//		for(Person person : persons) {
//			System.out.println(person);
//		}
		
		try (FileOutputStream fos = new FileOutputStream("myFile.data");
				ObjectOutputStream output = new ObjectOutputStream(fos)) {
			output.writeObject(persons);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		List<Person> persons2 = null;
		try (FileInputStream fis = new FileInputStream("myFile.data");
				ObjectInputStream input = new ObjectInputStream(fis)) {
			persons2 = (List<Person>)input.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		for (Person person : persons2) {
			System.out.println(person);
		}
	}

	static List<Person> getRandomPersons() {
		List<Person> persons = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			Person person = new Person(i, "Name" + i, 
										"Address" + i, LocalDate.of(2000, 1, i));
			persons.add(person);
		}
		return persons;
	}
}